import React from "react";
import { View, Text, TouchableOpacity, FlatList } from "react-native";

const TaskList = ({ navigation }) => {
  const tasks = [
    {
      id: 1,
      title: "Ir ao Mercado",
      date: "2024-02-27",
      time: "10:00",
      address: "Super SS",
    },
    {
      id: 2,
      title: "Conferência Anual de Inovação",
      date: "2024-03-15",
      time: "09:00",
      address: "Centro de Convenções Futurum",
    },
    {
      id: 3,
      title: "Lançamento do Novo Produto X",
      date: " 2024-04-10",
      time: "14:00",
      address: "Salão de Eventos Aurora",
    },
  ];
  const taskPress = (task) => {
    navigation.navigate("DetalhesDasTarefas", { task });
  };
  return (
    <View>
      <FlatList
        data={tasks}
        keyExtractor={(item) => item.id.toString}
        renderItem={({ item }) => (
          <TouchableOpacity onPress={() => taskPress(item)}>
            <Text>{item.title}</Text>
          </TouchableOpacity>
        )}
      />
    </View>
  );
};
export default TaskList;
